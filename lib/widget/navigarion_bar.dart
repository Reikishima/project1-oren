import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:v1/Gallery/gallery_view.dart';
import 'package:v1/Profile/profile.dart';
import 'package:v1/widget/nav_controller.dart';
import '../home/home_view.dart';
import '../utils/app_colors.dart';
import '../controller/nav_controller.dart';

class NavigationPage extends StatelessWidget {
  BottomNavigationController bottomNavigationController =
      BottomNavigationController();

  NavigationPage({super.key});
  final screens = [
    const HomeView(),
    GalleryView(),
    const Profile()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => IndexedStack(
          index: bottomNavigationController.selectedIndex.value,
          children: screens,
        ),
      ),
      bottomNavigationBar: Obx(() => BottomNavigationBar(
            backgroundColor: AppColors.darkFlatGold,
            type: BottomNavigationBarType.fixed,
            selectedItemColor: AppColors.darkBackground,
            unselectedItemColor: AppColors.greyDisabled,
            showSelectedLabels: true,
            onTap: (index) {
              bottomNavigationController.changeTabIndex(index);
            },
            currentIndex: bottomNavigationController.selectedIndex.value,
            items: [
              BottomNavigationBarItem(
                  icon: const Icon(Icons.home),
                  label: "Home",
                  backgroundColor: AppColors.darkFlatGold),
              BottomNavigationBarItem(
                  icon: Image.asset(
                    'assets/icons/gallery.png',
                    width: 20,
                    height: 20,
                  ),
                  label: "Gallery",
                  backgroundColor: AppColors.darkFlatGold),
              BottomNavigationBarItem(
                  icon: const Icon(Icons.people),
                  label: "Profile",
                  backgroundColor: AppColors.darkFlatGold)
            ],
          )),
    );
  }
}
