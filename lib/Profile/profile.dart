import 'package:flutter/material.dart';
import 'package:v1/utils/app_colors.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.darkBackground,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_rounded),
          onPressed: () {},
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: Image.asset(
                'assets/images/blackowl-transparent.png',
                width: 50,
                height: 50,
              ))
        ],
        title: Text(
          'Profile Information',
          style: TextStyle(color: AppColors.darkFlatGold),
        ),
      ),
      backgroundColor: AppColors.darkBackground,
      body: Container(
        padding: const EdgeInsets.only(left: 50, top: 8),
        child: Column(
          children: [
            SizedBox(
              width: 300,
              height: 450,
              child: Stack(
                children: [
                  Center(
                    child: Container(
                      height: 375,
                      width: 450,
                      decoration: BoxDecoration(
                        color: AppColors.darkFlatGold,
                        borderRadius: BorderRadius.circular(18),
                      ),
                      child: forms(context)
                    ),
                  ),
                  Positioned(
                    top: 0,
                    right: 40,
                    left: 40,
                    bottom: 300,
                    child: Container(
                      
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                        color: AppColors.darkFlatGold,
                        shape: BoxShape.circle,
                        border: Border.all(color: AppColors.darkBackground),
                      ),
                      child: const CircleAvatar(
                        backgroundImage: AssetImage('assets/images/ROG.png'),
                        radius: 30,
                      ),
                    
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                width: 300,
                height: 40,
                child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: AppColors.darkFlatGold,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    onPressed: () {},
                    child: Text(
                      'Edit Profile',
                      style: TextStyle(color: AppColors.darkBackground),
                    )),
              ),
            ),
            SizedBox(
              width: 300,
              height: 40,
              child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: AppColors.darkFlatGold,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                  onPressed: () {},
                  child: Text(
                    'Verify Account',
                    style: TextStyle(color: AppColors.darkBackground),
                  )),
            )
          ],
        ),
      ),
    );
  }
}

Widget forms(BuildContext context) {
  return Container(
    width: 285,
    
    padding: const EdgeInsets.only(top: 120),
    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Row(
        
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [Icon(Icons.phone), Text('983999999',),
        ],
      ),
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [
          Icon(Icons.people), Text('AAAAAAAA',
               ),
          
        ],
      ),
      Row(
          crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [
          Icon(Icons.location_on),
           Text('AAAAAAA',
                textAlign: TextAlign.end),
        ],
      ),
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [
          Icon(Icons.email),Text('aaaaaaaa')
        ],
      )
    ]),
  );
}
