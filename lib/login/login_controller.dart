import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:v1/controller/nav_controller.dart';
import 'package:v1/widget/navigarion_bar.dart';

import '../home/home_controller.dart';
import '../home/home_view.dart';

class LoginController extends GetxController{
  final loginformkey = GlobalKey<FormState>();
  ScrollController scrollController = ScrollController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final email = '';
final password= '';

  void login(){
   if (emailController != email && passwordController !=password ){
        Get.off(  NavigationPage());
      }else{
        Get.put(NavigationController());
      }
  }
}