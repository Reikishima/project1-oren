import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:v1/home/home_view.dart';
import 'package:v1/login/login_controller.dart';
import 'package:v1/utils/app_colors.dart';

import '../register/register_view.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Form(
          child: Container(
            padding: const EdgeInsets.all(50.0),
            color: AppColors.darkBackground,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/blackowl-transparent.png',
                  width: 200,
                  height: 200,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: AppColors.darkFlatGold),
                    ),
                    hintText: 'Login Dengan Email',
                    hintStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                        color: AppColors.darkFlatGold),
                  ),
                  style: TextStyle(
                      color: AppColors.darkFlatGold,
                      fontSize: 14,
                      fontFamily: 'Times New Roman',
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  width: 300,
                  child: TextButton(
                    style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        backgroundColor: AppColors.darkFlatGold),
                    onPressed: () {
                      controller.login();
                    },
                    child: Text(
                      'Login',
                      style: TextStyle(
                        color: AppColors.darkBackground,
                      ),
                    ),
                  ),
                ),
                 SizedBox(
                  width: 300,
                  child: TextButton(
                    style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        backgroundColor: AppColors.darkFlatGold),
                    onPressed: () {
                     Get.to( RegisterView());
                    },
                    child: Text(
                      'Register',
                      style: TextStyle(
                        color: AppColors.darkBackground,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
