import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:v1/utils/app_colors.dart';
import '../splash/splash_controller.dart';

class SplashView extends GetView<SplashController>{
  const SplashView({Key? key}) : super(key: key); 
  @override 
  Widget build(BuildContext context){
    return MaterialApp(
      home: Image.asset('assets/images/blackowl-transparent.png',height: 50,width: 50,),
      color: AppColors.darkBackground,
      
    
    );
  }
}