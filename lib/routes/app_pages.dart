import 'package:get/get.dart';
import 'package:v1/home/home_binding.dart';
import 'package:v1/login/login_binding.dart';
import 'package:v1/splash/splash_binding.dart';

import '../home/home_view.dart';
import '../login/login_view.dart';
import '../splash/splash_view.dart';

part 'app_routes.dart';

class AppPages{
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(name: _Paths.HOME, page: ()=> const HomeView(),binding: HomeBinding()),
    GetPage(name: _Paths.SPLASH, page: ()=> const SplashView(),binding: SplashBinding()),
    GetPage(name: _Paths.LOGIN, page: ()=> const LoginView(),binding: LoginBinding()),
    //GetPage(name: _Paths.REGISTER, page: ()=> const RegisterView(),)
  ];
}