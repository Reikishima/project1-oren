import 'package:get/get.dart';
import '../controller/nav_controller.dart';
import '../home/home_controller.dart';

class NavigationBinding extends Bindings{
  @override 
  void dependencies(){
    Get.lazyPut<NavigationController>(() => NavigationController());
    Get.lazyPut<HomeController>(() => HomeController());
    //Get.lazyPut<NavigationController>(() => NavigationController());
  }
}