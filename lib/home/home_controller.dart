

import 'package:get/get.dart';
// ignore: depend_on_referenced_packages
import 'package:get_storage/get_storage.dart';

import '../../routes/app_pages.dart';


class HomeController extends GetxController{
  final getStorage = GetStorage();
  var name = '';
  @override
  void onInit(){
    super.onInit();
  name = getStorage.read('name');  
  }




  logout(){
    getStorage.erase();
    Get.offAllNamed(Routes.LOGIN);
  }
}