import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:v1/utils/app_colors.dart';


class HomeView extends StatelessWidget {
  const HomeView({super.key});
  void main() => runApp(const GetMaterialApp(home: HomeView()));

// ignore: camel_case_types

  static const appTitle = "Task 2";

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        backgroundColor: AppColors.darkBackground,
        body: SafeArea(
          child: Container(padding: const EdgeInsets.all(8.0),
            child: Center(
          child: Column(
            children: <Widget>[
              Image.asset('assets/images/black_owl_large.png',
                  width: 275, height: 75),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 0, bottom: 10),
                    child: SizedBox(
                      width: 175,
                      height: 50,
                      child: TextButton.icon(
                        label: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Juan Bruen\n',
                                style: TextStyle(
                                    color: AppColors.whiteSkeleton,
                                    fontSize: 14),
                              ),
                              TextSpan(
                                text: 'VVIP',
                                style: TextStyle(
                                    color: AppColors.darkGold,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {},
                        style: TextButton.styleFrom(
                            backgroundColor: AppColors.darkBackground),
                        icon: CircleAvatar(
                          backgroundImage:
                              const AssetImage('assets/icons/profile.png'),
                          backgroundColor: AppColors.darkBackground,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 175),
                    child: Icon(
                      Icons.notifications,
                      color: AppColors.greyDisabled,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 23),
                child: Row(
                  children: [
                    SizedBox(
                      width: 175,
                      height: 50,
                      child: ElevatedButton.icon(
                        label: Text(
                          ' Voucher\n Rp.735.000,-',
                          style: TextStyle(
                            color: AppColors.darkBackground,
                          ),
                        ),
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.darkFlatGold,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              bottomLeft: Radius.circular(10),
                            ),
                          ),
                        ),
                        icon: Image.asset(
                          'assets/icons/voucher.png',
                          width: 20,
                          height: 20,
                          color: AppColors.darkBackground,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 175,
                      height: 50,
                      child: ElevatedButton.icon(
                        label: Text(
                          ' Point\n 500.000 pts',
                          style: TextStyle(
                            color: AppColors.darkBackground,
                          ),
                        ),
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.darkFlatGold,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(10),
                            ),
                          ),
                        ),
                        icon: Image.asset(
                          'assets/icons/point.png',
                          width: 20,
                          height: 20,
                          color: AppColors.darkBackground,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 350,
                  height: 50,
                  child: OutlinedButton.icon(
                    label: Text(
                      'My E-Tikcet',
                      style: TextStyle(color: AppColors.darkGold),
                    ),
                    icon: Image.asset(
                      'assets/icons/voucher.png',
                      width: 20,
                      height: 20,
                      color: AppColors.darkGold,
                    ),
                    onPressed: () {},
                    style: OutlinedButton.styleFrom(
                      side: BorderSide(width: 2, color: AppColors.darkFlatGold),
                      backgroundColor: AppColors.darkBackground,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: SizedBox(
                  child: Center(
                    child: ListView(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20, top: 20,left: 8),
                          child: Text(
                            'Upcoming',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: AppColors.whiteSkeleton),
                          ),
                        ),
                        SizedBox(
                          height: 150,
                          child: ListView(
                            physics: const BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(
                                  left: 20,
                                ),
                                width: 300,
                                height: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Colors.black12,
                                  image: const DecorationImage(
                                    image: NetworkImage(
                                      "https://images.unsplash.com/photo-1607355739828-0bf365440db5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1444&q=80",
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                  left: 20,
                                ),
                                width: 300,
                                height: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: const DecorationImage(
                                    image: NetworkImage(
                                      "https://images.pexels.com/photos/2583852/pexels-photo-2583852.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260",
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                  left: 20,
                                ),
                                width: 300,
                                height: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: const DecorationImage(
                                    image: NetworkImage(
                                      "https://images.unsplash.com/photo-1584810359583-96fc3448beaa?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80",
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: Text(
                        'Packages',
                        style: TextStyle(
                            color: AppColors.whiteSkeleton,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 150,
                      width: 100,
                      child: ListView(
                        physics: const BouncingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        children: [
                          InkWell(
                            splashColor: AppColors.darkBackground,
                            onTap: () {},
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Ink.image(
                                  image: const AssetImage(
                                      'assets/images/logo_bo.png'),
                                  height: 100,
                                  width: 100,
                                  fit: BoxFit.cover,
                                ),
                                Center(
                                  child: SizedBox(
                                    child: Text(
                                      ' LOGO BO\nRp.500.000',
                                      style: TextStyle(
                                          color: AppColors.darkFlatGold),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 6,
                                )
                              ],
                            ),
                          ),
                          InkWell(
                            splashColor: AppColors.darkBackground,
                            onTap: () {},
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Ink.image(
                                  image: const AssetImage(
                                      'assets/images/logo_bo.png'),
                                  height: 100,
                                  width: 100,
                                  fit: BoxFit.cover,
                                ),
                                Center(
                                  child: SizedBox(
                                    child: Text(
                                      ' LOGO BO\nRp.500.000',
                                      style: TextStyle(
                                          color: AppColors.darkFlatGold),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 6,
                                )
                              ],
                            ),
                          ),
                          InkWell(
                            splashColor: AppColors.darkBackground,
                            onTap: () {},
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Ink.image(
                                  image: const AssetImage(
                                      'assets/images/logo_bo.png'),
                                  height: 100,
                                  width: 100,
                                  fit: BoxFit.cover,
                                ),
                                Center(
                                  child: SizedBox(
                                    child: Text(
                                      ' LOGO BO\nRp.500.000',
                                      style: TextStyle(
                                          color: AppColors.darkFlatGold),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 6,
                                )
                              ],
                            ),
                          ),
                          InkWell(
                            splashColor: AppColors.darkBackground,
                            onTap: () {},
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Ink.image(
                                  image: const AssetImage(
                                      'assets/images/logo_bo.png'),
                                  height: 100,
                                  width: 100,
                                  fit: BoxFit.cover,
                                ),
                                Center(
                                  child: SizedBox(
                                    child: Text(
                                      ' LOGO BO\nRp.500.000',
                                      style: TextStyle(
                                          color: AppColors.darkFlatGold),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 6,
                                )
                              ],
                            ),
                          ),
                          InkWell(
                            splashColor: AppColors.darkBackground,
                            onTap: () {},
                            child: Column(mainAxisSize: MainAxisSize.min,
                              children: [
                                Ink.image(
                                  image:
                                      const AssetImage('assets/images/logo_bo.png'),
                                  height: 100,
                                  width: 100,
                                  fit: BoxFit.cover,
                                ),
                               Center(
                                 child: SizedBox(
                                          child: Text(
                                      ' LOGO BO\nRp.500.000',
                                      style: TextStyle(color: AppColors.darkFlatGold),
                                    ),
                                        ),
                               ),const SizedBox(height: 6,)
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
          )),
      ),
    );
  }
}
