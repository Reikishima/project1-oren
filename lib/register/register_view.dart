import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:v1/login/login_view.dart';
import 'package:v1/utils/app_colors.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class RegisterView extends StatelessWidget {
  RegisterView({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.darkBackground,
          title: Text(
            'Register',
            style: TextStyle(color: AppColors.darkFlatGold),
          ),
          actions: [
            IconButton(
              icon: Image.asset(
                'assets/images/blackowl-transparent.png',
                height: 100,
                width: 100,
              ),
              onPressed: () {},
            )
          ],
          leading: IconButton(
            onPressed: () {
              Get.to(const LoginView());
            },
            icon: const Icon(Icons.arrow_back_ios_new),
            color: AppColors.darkFlatGold,
          ),
        ),
        backgroundColor: AppColors.darkBackground,
        body: Form(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                forms,
                dropdown,
                button,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget dropdown = Container(
    padding: const EdgeInsets.all(8.0),
    child: Column(children: [
      DropdownSearch<String>(
        onChanged: print,
        items: const [
          'Male',
          'Female',
        ],
        selectedItem: '',
        dropdownDecoratorProps: DropDownDecoratorProps(
          dropdownSearchDecoration: InputDecoration(
              labelText: 'Select gender',
              labelStyle: TextStyle(color: AppColors.darkBackground),
              hoverColor: AppColors.darkFlatGold,
              hintText: 'Select Gender',
              filled: true,
              hintStyle: TextStyle(
                  color: AppColors.darkFlatGold,
                  backgroundColor: AppColors.darkBackground),
              fillColor: AppColors.whiteSkeleton),
          baseStyle: TextStyle(color: AppColors.whiteSkeleton),
        ),
        popupProps: PopupProps.bottomSheet(
            bottomSheetProps: BottomSheetProps(
          backgroundColor: AppColors.darkFlatGold,
        )),
      ),
      DropdownSearch<String>(
        onChanged: print,
        items: const ['Jakarta', 'Bandung', 'Serang'],
        selectedItem: '',
        dropdownDecoratorProps: DropDownDecoratorProps(
          dropdownSearchDecoration: InputDecoration(
              labelText: 'Select City',
              labelStyle: TextStyle(color: AppColors.darkBackground),
              hoverColor: AppColors.darkFlatGold,
              hintText: 'Select Gender',
              filled: true,
              hintStyle: TextStyle(
                  color: AppColors.darkFlatGold,
                  backgroundColor: AppColors.darkBackground),
              fillColor: AppColors.whiteSkeleton),
          baseStyle: TextStyle(color: AppColors.whiteSkeleton),
        ),
        popupProps: PopupProps.bottomSheet(
            bottomSheetProps: BottomSheetProps(
          backgroundColor: AppColors.darkFlatGold,
        )),
      ),
    ]),
  );
}

Widget forms = Form(
  child: Container(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: [
        TextField(
          decoration: InputDecoration(
              labelStyle: TextStyle(color: AppColors.darkFlatGold),
              labelText: 'First Name',
              hintText: 'fill your first name',
              hintStyle: TextStyle(color: AppColors.darkFlatGold),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.darkFlatGold))),
          style: TextStyle(
            color: AppColors.darkFlatGold,
          ),
        ),
        TextField(
          decoration: InputDecoration(
              labelStyle: TextStyle(color: AppColors.darkFlatGold),
              labelText: 'Last Name',
              hintText: 'fill your last name',
              hintStyle: TextStyle(color: AppColors.darkFlatGold),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.darkFlatGold))),
          style: TextStyle(
            color: AppColors.darkFlatGold,
          ),
        ),
        TextField(
          decoration: InputDecoration(
              labelStyle: TextStyle(color: AppColors.darkFlatGold),
              labelText: 'EmailAddress',
              hintText: 'fill your EmailAddress',
              hintStyle: TextStyle(color: AppColors.darkFlatGold),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.darkFlatGold))),
          style: TextStyle(
            color: AppColors.darkFlatGold,
          ),
        ),
        TextField(
          decoration: InputDecoration(
              labelStyle: TextStyle(color: AppColors.darkFlatGold),
              labelText: 'Phone Number',
              hintText: 'fill your PhoneNumber',
              hintStyle: TextStyle(color: AppColors.darkFlatGold),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.darkFlatGold))),
          style: TextStyle(
            color: AppColors.darkFlatGold,
          ),
        ),
        TextField(
          decoration: InputDecoration(
              labelStyle: TextStyle(color: AppColors.darkFlatGold),
              labelText: 'Address',
              hintText: 'fill your Address',
              hintStyle: TextStyle(color: AppColors.darkFlatGold),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: AppColors.darkFlatGold))),
          style: TextStyle(
            color: AppColors.darkFlatGold,
          ),
        ),
      ],
    ),
  ),
);
Widget button = SizedBox(
    width: 350,
    child: TextButton(
      style: TextButton.styleFrom(
          backgroundColor: AppColors.darkFlatGold,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20
              ),
              ),
              ),
      onPressed: () {
        Get.to(const LoginView());
      },
      child: Text(
        'Register',
        style: TextStyle(color: AppColors.darkBackground),
      ),
    ));
